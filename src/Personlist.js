import React, {Component} from 'react';
import axios from 'axios';

export default class Personlist extends Component {
    state = {
        persons: []
    }
    componentDidMount(){
        axios.get('http://jsonplaceholder.typicode.com/users').then(response => {
          this.setState({ persons: response.data });
        })
      }

      render() {
          return(
              <div>
                  <ul>
                    {this.state.persons.map(person => <li>{person.name} </li>)}
                  </ul>
              </div>
          )
      }
}