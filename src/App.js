import React, { Component } from "react";
import "./App.css";
// import uuid from "uuid";
import Grid from "@material-ui/core/Grid";
import { Input } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Todos from "./Components/Todo";
import Select from "./Components/Select";
import $ from "jquery";

class App extends Component {
  constructor() {
    super();
    this.state = {
      project: [
        {
          title: "Learn ReactJs",
          category: "Javascript"
        },
        {
          title: "Website of Business",
          category: "Facebook"
        },
        {
          title: "E-commerce Shopping",
          category: "Web Development"
        }
      ],
      category: [
        {
          id: 1,
          name: "Javascript"
        },
        {
          id: 2,
          name: "Facebook"
        },
        {
          id: 3,
          name: "Web Development"
        }
      ],
      selectedCategory: "",
      task: "",
      todos: []
    };
    this.getListItem = this.getListItem.bind(this);
    this.getTodos = this.getTodos.bind(this);
    this.getFromFetchMethod = this.getFromFetchMethod.bind(this);

    // this.onDelete = this.onDelete.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  getTodos() {
    $.ajax({
      url: "https://jsonplaceholder.typicode.com/posts",
      dataType: "json",
      cache: false,
      success: function(data) {
        this.setState({ todos: data });
      }.bind(this),
      error: function(xhr, status, err) {
        console.log(err);
      }
    });
  }

  onChange = ({ target }) => {
    const { value, name } = target;
    switch (name) {
      case "task":
        this.setState({
          ...this.state,
          [name]: value
        });
        break;

      case "category":
        this.setState(prevState => {
          return { ...prevState, selectedCategory: value };
        });
        console.log(this.state.selectedCategory);

        break;
      default:
    }
  };
  onDelete = id => {
    const newList = this.state.project;
    newList.splice(id, 1);
    this.setState({ project: newList });
  };
  getListItem = () => {
    console.log(this.state.project);
  };

  componentDidMount() {
    // this.getTodos();
    this.getFromFetchMethod();
  }
  render() {
    return (
      <div className="App">
        <Grid container spacing={10}>
          <Grid item xs={12}>
            <h1> Todo List Part 2</h1>
            <hr />
          </Grid>
        </Grid>
        <Grid container spacing={4}>
          <Grid item xs={2}>
            <Input
              type="text"
              name="task"
              ref="inputRef"
              id="text"
              placeholder="Enter Task"
              onChange={this.onChange}
              value={this.state.task}
            />
          </Grid>
          <Grid item xs={2}>
            <Select name="category" category={this.state.category} />
          </Grid>
          <Grid item xs={8}>
            <Button id="btn" variant="contained" color="primary">
              Add New Task
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Todos
              project={this.state.project}
              onClick={this.onDelete.bind(this)}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <h3> API todo list</h3>
          <hr />

          <table>
            <thead>
              <tr>
                <th align="center">ID</th>
                <th align="center">Title</th>
                <th align="center">Completed</th>
              </tr>
            </thead>
            {this.state.todos.map((row, index) => (
              <tbody key={index}>
                <tr>
                  <td> {row.id}</td>
                  <td> {row.title}</td>
                  <td> {row.completed ? "true" : "false"}</td>
                </tr>
              </tbody>
            ))}
          </table>
        </Grid>
      </div>
    );
  }
  getFromFetchMethod() {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({ todos: data });
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export default App;
