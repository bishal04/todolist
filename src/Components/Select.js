import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  formControl: {
    marginTop: theme.spacing(-2),
    marginRight: theme.spacing(-2),
    marginLeft: theme.spacing(-2),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

export default function SimpleSelect(props) {
  const classes = useStyles();
  const [Category, setCategory] = React.useState("");

  const handleChange = event => {
    const { value } = event.target;
    setCategory(value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">Category</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={Category}
          onChange={handleChange}
          // labelWidth={labelWidth}
        >
          {props.category.map((value, index) => (
            <MenuItem key={index} value="value.name">
              {value.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
