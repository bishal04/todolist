import React from "react";
import Button from "@material-ui/core/Button";

function Todos(props) {
  return (
    <div>
      <h3> List of Project </h3>
      {props.project.map((item, index) => (
        <div key={index}>
          <li>
            {item.title} {item.Category}
            <Button color="secondary" onClick={props.onClick}>
              Delete
            </Button>
            {/* <button onClick={props.onClick}>Delete</button> */}
          </li>
        </div>
      ))}
    </div>
  );
}
export default Todos;
